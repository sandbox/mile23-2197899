NetBeansDrupalComposed
---

What?
---

This is a tiny project consisting mainly of a `composer.json` file and some instructions.

When you use Composer to install the dependencies of this project, you'll have a lot of what you need to configure NetBeans to do semi-automatic coding standards reviews.

How?
----

NetBeansDrupalComposed is a Composer project. This means you can install it without downloading anything (except maybe Composer).

If you already have Composer installed globally, you can just go ahead and use it. If you don't, you'll need to do something like this:

	# Add a scratchpad directory.
	mkdir some_dir
	cd some_dir
	curl -sS https://getcomposer.org/installer | php

Now that you have Composer, you can use it:

	./composer.phar create-project mile23/netbeansdrupalcomposed -s dev

This will create a new directory called netbeansdrupalcomposed, and install everything into it.

The install process tells you some paths to keep in mind. These are the paths we'll configure in NetBeans. Leave that window open, or copy-paste it somewhere, or keep it in mind.

On my computer it looks like this:

	Checking...
	
	Paths to configure in NetBeans:
	  PHPCS script: /Users/paul/netbeansdrupalcomposed/bin/phpcs
	  Drupal standard: /Users/paul/netbeansdrupalcomposed/vendor/drupal/coder/coder_sniffer/Drupal/
	  PHPMD script: /Users/paul/netbeansdrupalcomposed/bin/phpmd
	  PHPCPD script: /Users/paul/netbeansdrupalcomposed/bin/phpcpd
	  PDepend script: /Users/paul/netbeansdrupalcomposed/bin/pdepend

If you missed this path information or want to redisplay it again for whatever reason, you can just say `php scripts/pathrevealer.php`.

Now we can configure all this stuff inside NetBeans.

NetBeans
---

Let's talk about NetBeans.

You can get it here:https://netbeans.org/downloads/

You'll probably want to download the PHP+HTML version.

The PHP version of NetBeans comes with its own code analysis tool. You can configure this to use PHP_CodeSniffer. However, it doesn't allow you to specify sniff standards that aren't installed within the phpcs source tree. Since the Drupal coding standards are buried inside the Coder module, we can't use this if we want to use Composer to manage our dependencies. Which we do. :-)

So instead we use the PHPCSMD plugin. NetBeans has a lot of plugins for all manner of uses. There are a number of PHP_CodeSniffer plugins... I prefer PHPCSMD, so that's what we're using in this project. Here is the info page for the plugin: http://plugins.netbeans.org/plugin/42434/phpcsmd

You install this plugin by running NetBeans and then going to Tools->Plugins and finding and installing it. Not too difficult. We can add more instruction here if people are confused.

Now you'll note that under Preferences->PHP section there is a tab titled PHPCSMD. This gives you a place to enter the paths we noted at the end of the previous section.

You probably also want to check the 'show annotations in error stripe' checkbox. It gives you a nice visual indicator.

Click 'Apply.'

Click 'OK.'

Open a PHP file (this is more cumbersome than it sounds...).

PHPCSMD will now tell you how many coding standards violations exist in that file. It will also highlight those lines in red or yellow. You can hover over the icon in the line number area to see what's wrong.

Edit the file, save it, and then right click it. You'll see an option to 'Check for violations.' Choose that and phpcs will do a re-evaluation.

And that's it. :-)
