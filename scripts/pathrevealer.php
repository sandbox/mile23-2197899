<?php

/**
 * @file
 * Tell the user what paths to configure within NetBeans.
 */

$root = dirname(dirname(__FILE__));

$paths = array(
  'PHPCS script' => $root . '/bin/phpcs',
  'Drupal standard' => $root . '/vendor/drupal/coder/coder_sniffer/Drupal/',
  'PHPMD script' => $root . '/bin/phpmd',
  'PHPCPD script' => $root . '/bin/phpcpd',
  'PDepend script' => $root . '/bin/pdepend',
);

echo "Checking...\n";

$errors = array();

foreach($paths as $name => $path) {
  if (!file_exists($path)) {
    $errors[$name] = $path;
  }
}

if (count($errors) > 0) {
  echo "\nWARNING: The following dependencies could not be found:\n";
  foreach ($errors as $name => $path) {
    unset($paths[$name]);
    echo "  $name: $path\n";
  }
}

if (count($paths) > 0) {
  echo "\nPaths to configure in NetBeans:\n";
  foreach($paths as $name => $path) {
    echo "  $name: $path\n";
  }
}

echo "\n";
